service cloud.firestore {

  // rules
  match /databases/{database}/documents {

    //
    // common functions
    //

    function auth() {
      return request.auth.uid != null;
    }

    function admin() {
      return exists(/databases/$(database)/documents/admins/$(request.auth.uid));
    }

    function stringCharLimit(str, limit) {
        return str is string && str.size() <= limit;
    }

    // return true if user doc's ref matches the auth.uid
    // thanks to: https://qiita.com/sgr-ksmt/items/1a731fdadf06119d35fc
    function matchUserReference(ref, userID) {
      // check only path (no GET)
      return ref != null
             && ref == /databases/$(database)/documents/users/$(userID);
    }

    match /bookmarks/{userID} {
      allow get, write: if request.auth.uid == userID;
    }
    match /notifications/{userID} {
      allow get, write: if request.auth.uid == userID;
    }

    //
    // stamp-related documents
    //

    match /stamps/{stamp} {
      allow create: if auth() &&
                       request.resource.data.status == "pending" &&
                       matchUserReference(request.resource.data.user, request.auth.uid);
    }

    match /stampSets/{stampSet} {
      allow get: if auth();
      allow list: if auth() && request.query.limit <= 20;
      allow write: if auth() && admin();
    }

    match /stampOrders/{stampOrder} {
      allow create: if auth() &&
                       request.resource.data.status == "pending" &&
                       matchUserReference(request.resource.data.user, request.auth.uid);
    }

    match /stampStocks/{userID} {
      allow get: if auth() && request.auth.uid == userID;
    }

    //
    // docs for unique username
    //

    // return true if the string contains only lowercase & numbers
    function validateUsername(value) {
      return value is string
             && value.matches('\\w+')
    }

    match /usernames/{userName} {
      allow get: if auth();
      allow create: if validateUsername(userName)
    }

    //
    // user's public & minimum info
    //

    match /users/{userID} {
      allow get: if auth();
      allow update: if request.auth.uid == userID;
    }

    //
    // chat related documents
    //

    match /messages/{message} {
      //TODO: Reject writing messages to old bottles
      allow create: if request.resource.data.size() == 7 &&
                       request.resource.data.bottle is path &&
                       request.resource.data.index is number &&
                       request.resource.data.text is string &&
                       request.resource.data.themes is list &&
                       request.resource.data.timestamp == request.time &&
                       request.resource.data.user is path &&
                       stringCharLimit(request.resource.data.text, 2048) &&
                       matchUserReference(request.resource.data.user, request.auth.uid);
      allow update: if matchUserReference(resource.data.user, request.auth.uid) &&
                       request.resource.data.text is string &&
                       stringCharLimit(request.resource.data.text, 2048);
      allow get: if auth();
      allow list: if auth() && resource.data.bottle != null && request.query.limit <= 220;
    }

    match /tags/{tag} {
      allow list: if auth() && resource.data.parent != null && request.query.limit <= 10;
      allow get: if auth();
    }

    match /bottles/{bottle} {
      allow get: if auth();
    }
  }
}
