---
meta:
  - name: description
    content: タグボトル内部の共通処理
---

# 共通ライブラリー

タグとテーマの相互変換など、サーバー側とクライアント側に共通する処理は共用ライブラリ `tomato-puree` として分離されています。

## PureeRef

Firestoreには固定されたスキーマ定義がなく、入れようと思えばどんなデータでも入ってしまいます。この仕様による混乱を避けるため、ドキュメントの内容をinterfaceにより型システムで管理するクラスを導入しました。

```typescript
export class PureeRef<AvailableProps, DocInterface> {
  constructor(public ref: DocumentReference) {}

  /*
    set
   */

  public async set(data: AvailableProps, options: SetOptions) {
    return this.ref.set(data, options);
  }

  /* ... */
}

export type MessageRef = PureeRef<DocTypes.IMessage, DocTypes.IMessageDocument>;
```
