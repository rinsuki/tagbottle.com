---
meta:
  - name: description
    content: タグボトルの開発資料について
---

# GitLab

タグボトルのリポジトリーは [GitLab](https://gitlab.com) で管理され、そのCIツールを活用しています。

## Let's Encrypt

GitLab Pagesでhttpsを利用するため [certbot](https://certbot.eff.org/) を利用します。

```bash
sudo certbot certonly --manual --agree-tos --server https://acme-v02.api.letsencrypt.org/directory --domain *.tagbottle.com -m $EMAIL --preferred-challenges dns
```

処理成功時の出力

```
IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/tagbottle.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/tagbottle.com/privkey.pem
   Your cert will expire on 2018-09-18. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

## Troubleshooting

### GitLab.comのサービスが正しく動かない場合

1. 有料プランを契約して [support.gitlab.com](https://support.gitlab.com) から依頼を投げる
2. 作り直す（リポジトリ全体をエクスポートし、同名リポジトリのないアカウントでインポートし、グループ宛に譲渡する）  
