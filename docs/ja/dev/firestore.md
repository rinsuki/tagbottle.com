---
meta:
  - name: description
    content: タグボトルを支えるFirestoreインフラ
---

# Firestore

タグボトルはFirestoreを利用して構築されており、そのルールベースの制御により多くの機能を実現しています。


## troubleshooting

### `exists` `get` を含む関数が想定どおり動かない

* 関数において変数を利用するためには、関数の記載位置に注意する必要があります。
* 例えば、 `database` 変数を利用できるのは `database` ワイルドカード定義後だけです。

```
// 正
service cloud.firestore {
  match /databases/{database}/documents { // <--- {database} マッチ
    function admin() {
      return exists(/databases/$(database)/documents/admins/$(request.auth.uid)); // <--- マッチ済みのdatabaseを利用
    }

    match /stampSets/{stampSet} {
      allow write: if admin();
    }
  }
}
```


```
// 誤
service cloud.firestore {
  function admin() {
    return exists(/databases/$(database)/documents/admins/$(request.auth.uid)); // <--- まだ存在しないdatabaseを利用（エラーにならない）
  }
  
  match /databases/{database}/documents { // <--- {database} マッチ
    match /stampSets/{stampSet} {
      allow write: if admin();
    }
  }
}
```
