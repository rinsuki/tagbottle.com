---
meta:
  - name: description
    content: ボトル実装の技術的詳細について
---

# ボトルの実装

ボトルとは、一定数ごとにメッセージを区切る概念です。インターネット掲示板における「スレッド」（1スレッド1000レスまでなどの制限があり、同一テーマの2スレッド目などと数えることができる概念）と類似しているかもしれません。

## ボトル構成要素

### 1. tags ドキュメント

```json
{
  "tags": {
    "tagbottle%2Fdev%2F": {
      "current_bottle": "bottles/81ab6094-d5bd-43c6-9ba4-3c6e445b016a",
      "current_index": 5
    }
  }
}
```

### 1. bottles ドキュメント

```json
{
  "bottles": {  
    "81ab6094-d5bd-43c6-9ba4-3c6e445b016a": {
      "tag": "tagbottle%2Fdev%2F",
      "index": 5
    }
  }
}
```

### 2. messages ドキュメント

```json
{
    "messages": {
        "77cb234d-9eb6-4c55-afcf-05aa663e12d5": {
            "tag": "tagbottle%2Fdev%2F",
            "bottle": "81ab6094-d5bd-43c6-9ba4-3c6e445b016a"
        }
    }
}
```

### 3. firestore rules

```
service cloud.firestore {
  match /databases/{database}/documents {
    match /messages/{message} {
      allow list: if resource.data.bottle != null;
      allow get, create: if request.auth.uid != null;
    }
    match /tags/{tag} {
      allow get;
    }
  }
}
```




























