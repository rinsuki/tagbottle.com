---
home: true
actionText: サイトの使い方を読む
actionLink: /ja/usage/
features:
- title: 話題そのものをフォロー
  details: 気になる話題を直接選択。知り合いのアカウントを監視する必要はありません。
- title: 厳格なコスト政策
  details: 適切な売上と低廉なコストによるサービスの継続性を重視。望まない改変からユーザーを守ります。
- title: オープンソース
  details: 誰でも無料で競合サービスを運用可能。運営者が腐敗したら、簡単に取って代わることができます。
footer: Apache v2 Licensed | Copyright © 2018-present tagbottle project 
---
