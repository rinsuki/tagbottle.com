module.exports = {
  dest: "public",

  locales: {
    "/ja/": {
      lang: 'ja-JP',
      title: 'tagbottle',
      description: '話題をフォローする軽量SNS'
    }
  },

  themeConfig: {
    repo: 'https://gitlab.com/tagbottle/tagbottle.com',
    docsDir: 'docs',
    editLinks: true,
    locales: {
      '/ja/': {
        selectText: '言語を選択',
        label: '日本語',
        editLinkText: 'このページを改善する',

        nav: [
          {
            text: '使い方',
            link: '/ja/usage/'
          },
          {
            text: '開発資料',
            link: '/ja/dev/'
          }
        ],
        sidebar: {
          '/ja/usage/': [
            '',
            'tag',
            'message',
            'bottle',
            'stamp',
          ],
          '/ja/dev/': [
            '',
            'bottle',
            'tomato-puree',
            'gitlab',
            'deps',
            'firestore',
            'fcm',
            'storage'
          ]
        }
      }
    }
  }
}
