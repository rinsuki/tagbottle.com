import createTag, {
  IProps as LibProps
} from "tomato-puree/admin/chat/createTag";
import { DocumentReference } from "tomato-puree/admin/client";

import { admin, functions } from "../common";

const db = admin.firestore();

export interface IResult {
  bottle: string;
  index: number;
  tag: string;
}

export const onCall = functions.https.onCall(
  async (data: LibProps, context): Promise<IResult> => {
    // create tag
    const result = await createTag(db, data);

    if (result.isLeft()) {
      const error = result.value;
      throw new functions.https.HttpsError("invalid-argument", error.message);
    }

    // return json
    const { tagData, tagRef } = result.value;
    const { currentBottle, currentIndex } = tagData;

    return {
      bottle: (currentBottle as DocumentReference).path,
      index: currentIndex!,
      tag: tagRef!.ref.path
    };
  }
);
