import * as firebaseAdmin from "firebase-admin";
import * as functions from "firebase-functions";
import "firebase-functions";

// prepare default admin.apps and another admin(adminSA) for operation which require service-account.json
// https://firebase.google.com/docs/admin/setup#initialize_multiple_apps

// admin.apps via default config
const defaultConfig = JSON.parse(process.env.FIREBASE_CONFIG as string);
const admin = firebaseAdmin.initializeApp(defaultConfig, "[DEFAULT]");

// admin.apps via service account
const serviceAccount = functions.config()["service-account"].json;
const serviceAccountConfig = JSON.parse(process.env.FIREBASE_CONFIG as string);
serviceAccountConfig.credential = firebaseAdmin.credential.cert(
  serviceAccount as firebaseAdmin.ServiceAccount
);
const adminSA = firebaseAdmin.initializeApp(
  serviceAccountConfig,
  "ServiceAccount"
);

const firestore = firebaseAdmin.firestore();
firestore.settings({ timestampsInSnapshots: true });

export { firebaseAdmin, admin, adminSA, firestore, functions };
