import createUser, {
  IProps as LibProps
} from "tomato-puree/admin/account/createUser";
import { admin, functions } from "../common";

export interface IProps {
  displayName: string;
  photoUrl: string;
  token: string;
}

export const onCall = functions.https.onCall(
  async (data: IProps, context): Promise<IProps> => {
    const uid = context.auth!.uid;
    const props: LibProps = { ...data, uid };
    const result = await createUser(admin.firestore(), props);

    if (result.isLeft()) {
      const error = result.value;
      throw new functions.https.HttpsError("invalid-argument", error.message);
    }

    // auto bookmark some tags
    const bookmarkRef = admin
      .firestore()
      .collection("bookmarks")
      .doc(uid);
    await bookmarkRef.set({
      tags: {
        "hello/world": true,
        tagbottle: true,
        "tagbottle/dev": true,
        "tagbottle/please/寿司": true,
        "tagbottle/バグ": true
      }
    });

    return data;
  }
);
