import { admin, adminSA, functions } from "../common";

import fs from "fs";
import mkdirp from "mkdirp-promise";
import os from "os";
import path from "path";
import sharp from "sharp";

// Thumbnail prefix added to file names.
const THUMB_PREFIX = "thumb_";

// Resize with sharp
const resizeImage = (
  tmpFilePath: string,
  destFilePath: string
): Promise<sharp.OutputInfo> => {
  return new Promise((resolve, reject) => {
    sharp(tmpFilePath)
      .resize(256, 256)
      .background({ r: 0, g: 0, b: 0, alpha: 0 })
      .embed()
      .toFormat("png")
      .toFile(destFilePath, (err, info) => {
        if (!err) {
          resolve(info);
        } else {
          reject(err);
        }
      });
  });
};

export const onFinalize = functions.storage
  .object()
  .onFinalize(async object => {
    // validate
    if (!object.name || !object.contentType) {
      console.error(new Error("object property is invalid"));
      return;
    }

    // Exit if this is triggered on a file that is not an image.
    const contentType: string = object.contentType;
    if (!contentType.startsWith("image/")) {
      console.log("This is not an image.");
      return null;
    }

    // Exit if the image is already a thumbnail.
    const filePath: string = object.name;
    const fileDir = path.dirname(filePath);
    const fileName = path.basename(filePath);
    if (fileName.startsWith(THUMB_PREFIX)) {
      console.log("Already a Thumbnail.");
      return null;
    }

    // File and directory paths.
    // TODO: delete old thumbnail
    const thumbnailFilePath = path.normalize(
      path.join(fileDir, `${THUMB_PREFIX}${Date.now()}.png`)
    );
    const tempLocalFile = path.join(os.tmpdir(), filePath);
    const tempLocalDir = path.dirname(tempLocalFile);
    const tempLocalThumbFile = path.join(os.tmpdir(), thumbnailFilePath);

    // get users uid from directory path
    const uid = path.basename(fileDir);

    // use service account to generate signed url
    const storage = adminSA.storage();
    const bucket = storage.bucket();
    const file = bucket.file(filePath);
    const thumbnailFile = bucket.file(thumbnailFilePath);
    const metadata = {
      contentType
      // To enable Client-side caching you can set the Cache-Control headers here. Uncomment below.
      // 'Cache-Control': 'public,max-age=3600',
    };

    // Download file from bucket.
    await mkdirp(tempLocalDir);
    const dlResult = await file.download({ destination: tempLocalFile });
    if (!dlResult) {
      console.error(new Error("download failed"));
      return;
    }

    // Generate a thumbnail using sharp
    const cvResult = await resizeImage(tempLocalFile, tempLocalThumbFile);
    if (!cvResult) {
      console.error(new Error("resize failed"));
      return;
    }

    // Uploading the Thumbnail.
    const upResult = await bucket.upload(tempLocalThumbFile, {
      destination: thumbnailFilePath,
      metadata
    });
    if (!upResult) {
      console.error(new Error("upload failed"));
      return;
    }

    // Once the image has been uploaded delete the local files to free up disk space.
    fs.unlinkSync(tempLocalFile);
    fs.unlinkSync(tempLocalThumbFile);

    // Error: Permission iam.serviceAccounts.signBlob is required to perform this operation on service account
    const thumbnailUrl = await thumbnailFile.getSignedUrl({
      action: "read",
      expires: "03-01-2500"
    });

    if (!thumbnailUrl) {
      console.error(new Error("thumbnail creation failed"));
      return;
    }

    return await admin
      .firestore()
      .collection("users")
      .doc(uid)
      .update({ photoUrl: thumbnailUrl[0] });
  });
