import "fp-ts";

import { admin, functions } from "../common";

import { RefGenerator } from "tomato-puree/admin/ref";
import applyMessageStampRequest from "tomato-puree/admin/stamp/applyMessageStampRequest";

export const onMessageStampRequestCreate = functions.firestore
  .document(
    "messages/{messageId}/stampSets/{stampSetId}/requests/{messageStampRequestId}"
  )
  .onCreate(async (snapshot, context) => {
    // get params from path
    const messageId: string = context.params.messageId;
    const stampSetId: string = context.params.stampSetId;

    // create refs
    const db = admin.firestore();
    const refGenerator = new RefGenerator(db);
    const messageRef = refGenerator.message(messageId);
    const stampSetRef = refGenerator.stampSet(stampSetId);

    // call function
    const props = {
      messageRef,
      stampSetRef
    };
    const result = await applyMessageStampRequest(db, props);

    // output error
    if (result.isLeft()) {
      console.log("props", props);
      console.error(result.value);
    }
  });

export default onMessageStampRequestCreate;
