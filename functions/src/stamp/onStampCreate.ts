import "fp-ts";

import { firestore } from "firebase-admin";
import { admin, functions } from "../common";

import { IStampDocument } from "tomato-puree/admin/firestoreDocType";
import { PureeRef, StampRef } from "tomato-puree/admin/ref/pureeRef";
import callConsumptionTransaction from "tomato-puree/admin/stamp/callConsumptionTransaction";

export const onStampCreate = functions.firestore
  .document("stamps/{stampId}")
  .onCreate(async (snapshot, context) => {
    // create refs
    const db = admin.firestore();
    const stampRef = new PureeRef(snapshot.ref) as StampRef;
    const stampDoc = snapshot.data() as IStampDocument;

    // call function
    const props = {
      stampDoc,
      stampRef,
      timestamp: firestore.FieldValue.serverTimestamp()
    };
    const result = await callConsumptionTransaction(db, props);

    // output error
    if (result.isLeft()) {
      console.log("props", props);
      console.error(result.value);
    }
  });

export default onStampCreate;
