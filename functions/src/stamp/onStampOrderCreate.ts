import "fp-ts";

import { firestore } from "firebase-admin";
import { admin, functions } from "../common";

import { PureeRef, StampOrderRef } from "tomato-puree/admin/ref/pureeRef";
import callPaymentTransaction from "tomato-puree/admin/stamp/callPaymentTransaction";

export const onStampOrderCreate = functions.firestore
  .document("stampOrders/{stampOrderId}")
  .onCreate(async (snapshot, context) => {
    const db = admin.firestore();
    const stampOrderRef = new PureeRef(snapshot.ref) as StampOrderRef;
    const props = {
      stampOrderRef,
      timestamp: firestore.FieldValue.serverTimestamp()
    };
    const result = await callPaymentTransaction(db, props);

    if (result.isLeft()) {
      const errorMessage = result.fold(
        e => `${e.code}: ${e.message}`,
        () => "unexpected"
      );
      console.log("props", props);
      console.error(new Error(errorMessage));
    }
  });

export default onStampOrderCreate;
