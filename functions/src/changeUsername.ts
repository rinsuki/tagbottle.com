import { admin, functions } from "./common";

export default functions.firestore
  .document("usernames/{username}")
  .onCreate(async (snap, context) => {
    const { user } = snap.data()!;
    const { username } = context.params;

    // get username(s)
    const docs = await admin
      .firestore()
      .collection("usernames")
      .where("user", "==", user)
      .get();

    // start batch
    const batch = admin.firestore().batch();

    // update user info
    const userRef = admin
      .firestore()
      .collection("users")
      .doc(user);
    batch.update(userRef, { username });

    // remove old usernames
    docs.forEach(async doc => {
      // ignore latest username
      if (doc.id === username) {
        return;
      }

      // delete
      batch.delete(doc.ref);
    });

    // commit the batch
    return batch.commit();
  });
