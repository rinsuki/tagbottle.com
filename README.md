# tagbottle.com

## How to contribute

### Create issues
1. Open [Board](https://gitlab.com/tagbottle/tagbottle.com/boards).
2. Click plus button and input issue title.
  * bug: Malfunctions that should be fixed
  * enhancement: Request for improvement
  
### Create merge request
1. Open the issue and assign it to you.
2. Click `Create merge request` button. GitLab automatically makes a new git-branch associated with the issue.
3. `git pull` the branch, `git commit` your codes and `git push` you commits to GitLab.
4. Open the merge request and click the `Resolve WIP status` button.
5. Click the `Merge when pipeline succeeds` button.
  * dev.tagbottle.com will be automatically updated by GitLab CI.
  
## How to launch togbottle on localhost
Install [node](https://nodejs.org/ja/) (>=10.0) and [yarn](https://yarnpkg.com/lang/ja/).
``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev
```

