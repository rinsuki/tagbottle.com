import VueRouter from "vue-router";
import { Store } from "vuex";
import Firebase from "firebase";

declare module "vue/types/vue" {
  interface Vue {
    $store: Store<any>;
    $router: VueRouter;
    $firebase: Firebase.app.App;
  }
}
