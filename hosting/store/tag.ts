import * as clientPuree from "tomato-puree/client";
import encodeThemesToTagString from "tomato-puree/util/convert/tag/encodeThemesToTagString";
import isInvalidThemes from "tomato-puree/util/validate/tag/isInvalidThemes";
import decodeTagStringToThemes from "tomato-puree/util/convert/tag/decodeTagStringToThemes";

import RefGenerator = clientPuree.Ref.RefGenerator;

import Vue from "vue";
import { IState, ITagState } from "./index";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { firebaseAction } from "vuexfire";
import { TagRef } from "tomato-puree/client/ref/pureeRef";

/*
 * utils
 */

/** generate key for tag state object
 *
 * @param {string[]} themes: themes of target tag
 * @return {string} key: string value to store data in state object
 */
function tagStateKey(themes: string[]): string {
  return encodeThemesToTagString(themes) as string;
}

/*
 * state
 */

export const state = (): ITagState => {
  return {};
};

/*
 * getters
 */

export interface ITagInfo {
  data: clientPuree.FirestoreDocType.ITag;
}
export const getters: GetterTree<ITagState, IState> = {
  info(
    state,
    getters,
    rootState,
    rootGetters
  ): (themes: string[]) => ITagInfo | null {
    return themes => {
      const tagData = rootGetters["binding/tag/data"](themes);
      return Object.assign({}, { data: tagData });
    };
  }
};

/*
 * actions
 */

export const actions: ActionTree<ITagState, IState> = {
  async addTag(
    { commit, dispatch, state, getters },
    props: { themes: string[] }
  ) {
    // validate & convert
    const { themes } = props;
    if (isInvalidThemes(themes)) {
      return;
    }

    // bind tag with vuexfire
    const db = this.$firebase.firestore();
    const refGenerator = new RefGenerator(db);
    const tagRef = refGenerator.tag(themes) as TagRef;
    dispatch(
      "binding/tag/bind",
      { themes, tagRef: tagRef.ref },
      { root: true }
    );
  },

  async incrementTagUnreadNum(
    { commit, dispatch },
    props: { encodedTag: string }
  ) {
    // decode & convert tag-string to key
    const themes = decodeTagStringToThemes(props.encodedTag);
    if (!themes) {
      return;
    }
    const key = tagStateKey(themes);

    // add tag (if needed)
    if (!state[key]) {
      await dispatch("addTag", { themes });
    }

    // increment
    commit("INCREMENT_UNREAD", { key });
  },

  async resetTagUnreadNum({ commit, dispatch }, props: { themes: string[] }) {
    const { themes } = props;
    const key = tagStateKey(themes);

    // add tag (if needed)
    if (!state[key]) {
      await dispatch("addTag", { themes });
    }

    // reset
    commit("RESET_UNREAD", { key });
  }
};
