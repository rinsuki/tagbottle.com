import encodeThemesToTagString from "tomato-puree/util/convert/tag/encodeThemesToTagString";
import { firebaseAction } from "vuexfire";
import * as firebase from "firebase";
import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IBindingTagState, IBindTagData, IState } from "~/store";

/** generate key for tag binding object
 *
 * @param {string[]} themes themes of target tag
 * @return {string} key string
 */
function bindingKey(themes: string[]): string {
  return encodeThemesToTagString(themes) as string;
}

/*
 * state
 */

export const state = (): IBindingTagState => {
  return {};
};

/*
 * getters
 */

export const getters: GetterTree<IBindingTagState, IState> = {
  data(state: IBindingTagState): (themes: string[]) => IBindTagData | null {
    return themes => {
      const key = bindingKey(themes);
      return state[key] as IBindTagData;
    };
  }
};

/*
 * actions
 */

export const actions: ActionTree<IBindingTagState, IState> = {
  bind: firebaseAction(
    async (
      { bindFirebaseRef, state, commit },
      props: { themes: string[]; tagRef: firebase.firestore.DocumentReference }
    ) => {
      const { themes, tagRef } = props;
      const key = encodeThemesToTagString(themes);
      if (!key || state[key]) {
        return;
      }

      commit("ADD_TAG_OBJECT", { key });

      await bindFirebaseRef(key, tagRef, { maxRefDepth: 0 });
    }
  )
};

/*
 * mutations
 */

export const mutations: MutationTree<IBindingTagState> = {
  ADD_TAG_OBJECT: (state, props: { key: string }) => {
    Vue.set(state, props.key, {});
  }
};
