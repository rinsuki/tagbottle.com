import { MESSAGES_QUERY_LIMIT } from "tomato-puree/constants";
import {
  IMessage,
  IMessageDocument,
  CommonType,
  IStampSummaryDocument
} from "tomato-puree/client/firestoreDocType";

import { firebaseAction } from "vuexfire";
import * as firebase from "firebase";
import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";

import { IBindingBottleMessagesState, IBindMessageData, IState } from "~/store";

import { uniq } from "lodash";

const bindKey = (
  bottlePath: string,
  type: "initial" | "latest" | "old"
): string => {
  return `${bottlePath}/${type}`;
};

/*
 * state
 */

export const state = (): IBindingBottleMessagesState => {
  return {};
};

/*
 * getters
 */

export const getters: GetterTree<IBindingBottleMessagesState, IState> = {
  data(state): (bottlePath: string) => IBindMessageData[] | null {
    return bottlePath => {
      let result: IBindMessageData[] = [];

      // 1. oldest messages
      const oldKey = bindKey(bottlePath, "old");
      const old = state[oldKey] as IBindMessageData[];
      if (old) {
        result = result.concat(old);
      }

      // 2. initial messages
      const initialKey = bindKey(bottlePath, "initial");
      const initial = state[initialKey] as IBindMessageData[];
      if (initial) {
        result = result.concat(initial);
      }

      // 3. latest messages
      const latestKey = bindKey(bottlePath, "latest");
      const latest = state[latestKey] as IBindMessageData[];
      if (latest) {
        result = result.concat(latest);
      }

      // join, deduplicate and return them!
      return uniq(result);
    };
  },

  isAllBind(state): (bottlePath: string) => boolean {
    return bottlePath => {
      // check if old key is bind
      const oldKey = bindKey(bottlePath, "old");
      return !!state[oldKey];
    };
  }
};

/*
 * actions
 */

export const actions: ActionTree<IBindingBottleMessagesState, IState> = {
  async bind(
    { state, commit, dispatch, getters },
    props: { bottleRef: firebase.firestore.DocumentReference }
  ) {
    const { bottleRef } = props;

    // 1. bind only latest 10 messages
    const startTime = this.$firebase.firestore.Timestamp.now();
    const initialMessagesRef = this.$firebase
      .firestore()
      .collection("messages")
      .where("bottle", "==", bottleRef)
      .orderBy("timestamp", "desc")
      .startAt(startTime)
      .limit(10);
    const initialKey = bindKey(bottleRef.path, "initial");
    const initialDispatch = dispatch("setMessagesBinding", {
      key: initialKey,
      messagesRef: initialMessagesRef
    });

    // 2. bind future messages
    const latestMessagesRef = this.$firebase
      .firestore()
      .collection("messages")
      .where("bottle", "==", bottleRef)
      .orderBy("timestamp", "desc")
      .endBefore(startTime)
      .limit(MESSAGES_QUERY_LIMIT);
    const latestKey = bindKey(bottleRef.path, "latest");
    const latestDispatch = dispatch("setMessagesBinding", {
      key: latestKey,
      messagesRef: latestMessagesRef
    });

    return Promise.all([initialDispatch, latestDispatch]);
  },

  async bindAll(
    { state, commit, dispatch, getters },
    props: { bottleRef: firebase.firestore.DocumentReference }
  ) {
    const { bottleRef } = props;
    const normalDispatches = dispatch("bind", props);

    // get oldest message
    const message = getters.data(bottleRef.path);
    const oldestInitialMessage = message[message.length - 1];

    // 3. bind old messages
    const oldMessagesRef = this.$firebase
      .firestore()
      .collection("messages")
      .where("bottle", "==", bottleRef)
      .orderBy("timestamp", "desc")
      .startAfter(oldestInitialMessage.timestamp)
      .limit(MESSAGES_QUERY_LIMIT);
    const oldDispatch = dispatch("setMessagesBinding", {
      key: bindKey(bottleRef.path, "old"),
      messagesRef: oldMessagesRef
    });

    return Promise.all([normalDispatches, oldDispatch]);
  },

  async setMessagesBinding(
    { commit, dispatch, state },
    props: { key: string; messagesRef: firebase.firestore.Query }
  ) {
    const { key, messagesRef } = props;

    // ignore & return if the key is already bind
    if (state[key]) {
      return;
    }

    // add array in state
    commit("ADD_EMPTY_ARRAY", { key });
    messagesRef.onSnapshot((snapshot: firebase.firestore.QuerySnapshot) => {
      snapshot.docChanges().forEach(change => {
        // get data from change data
        const { type, doc, newIndex, oldIndex } = change;
        const message = doc.data() as IMessage;

        // add message to array, and bind user
        if (type === "added") {
          commit("ADD_MESSAGE", { key, index: newIndex, message, id: doc.id });
          dispatch(
            "binding/user/bind",
            { userRef: message.user as firebase.firestore.DocumentReference },
            { root: true }
          );
        }

        // update message in array
        if (type === "modified") {
          commit("MODIFY_MESSAGE", {
            key,
            index: newIndex,
            message,
            id: doc.id
          });
        }
      });
    });
  }
};

/*
 * mutations
 */

const convert = (message: IMessageDocument, id: string): IBindMessageData => {
  // TODO: workaround for only dev, old broken message doc's stamps may be object
  let stamps: IStampSummaryDocument[] = [];
  if (Array.isArray(message.stamps)) {
    stamps = message.stamps;
  }

  return {
    id,
    index: message.index as number,
    text: message.text as string,
    timestamp: message.timestamp as CommonType.ITimestamp,
    stamps: stamps.map(s => {
      return {
        amount: s.amount,
        stampItemId: s.stampItemId,
        stampSet: s.stampSet.id
      };
    }),
    themes: message.themes as string[],
    userId: (message.user as firebase.firestore.DocumentReference).id
  };
};

export const mutations: MutationTree<IBindingBottleMessagesState> = {
  ADD_EMPTY_ARRAY(state, { key }: { key: string }) {
    Vue.set(state, key, []);
  },

  ADD_MESSAGE(
    state,
    props: { key: string; index: number; message: IMessageDocument; id: string }
  ) {
    const { index, message, key, id } = props;
    if (state[key] === null) {
      return;
    }

    const data = convert(message, id);
    (state[key] as IBindMessageData[]).splice(index, 0, data);
  },

  MODIFY_MESSAGE(
    state,
    props: { key: string; index: number; message: IMessageDocument; id: string }
  ) {
    const { index, message, key, id } = props;
    if (state[key] === null) {
      return;
    }

    const data: IBindMessageData = convert(message, id);
    (state[key] as IBindMessageData[]).splice(index, 1, data);
  }
};
