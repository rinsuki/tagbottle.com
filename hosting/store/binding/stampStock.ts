import { ActionTree, MutationTree } from "vuex";
import { IBindingStampStockState, IState } from "~/store";
import { IStampStockDocument } from "tomato-puree/client/firestoreDocType/stampStock";
import { RefGenerator, StampStockRef } from "tomato-puree/client/ref";

type IModuleState = IBindingStampStockState;

/*
 * state
 */

export const state = (): IBindingStampStockState => {
  return {
    favorites: [],
    latestUsed: [],
    latestBought: []
  };
};

/*
 * actions
 */

export const actions: ActionTree<IModuleState, IState> = {
  async bind(
    { state, commit, dispatch },
    props: { stampStockRef: StampStockRef }
  ) {
    props.stampStockRef.ref.onSnapshot(
      snapshot => {
        const data = snapshot.data() as IStampStockDocument;
        commit("UPDATE", data);

        // get util
        const db = this.$firebase.firestore();
        const refGenerator = new RefGenerator(db);

        // bind all stamp-set in user's stamp-stock
        for (const item of data.favorites) {
          const stampSetRef = refGenerator.stampSet(item.stampSet.id);
          dispatch("binding/stampSet/bind", { stampSetRef }, { root: true });
        }
        for (const item of data.latestBought) {
          const stampSetRef = refGenerator.stampSet(item.stampSet.id);
          dispatch("binding/stampSet/bind", { stampSetRef }, { root: true });
        }
        for (const item of data.latestUsed) {
          const stampSetRef = refGenerator.stampSet(item.stampSet.id);
          dispatch("binding/stampSet/bind", { stampSetRef }, { root: true });
        }
      },
      error => {
        console.error(error);
      }
    );
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<IModuleState> = {
  UPDATE(state, props: IStampStockDocument) {
    state.favorites = props.favorites.map(f => {
      return { stampSet: f.stampSet.id, amount: f.amount };
    });
    state.latestUsed = props.latestUsed.map(f => {
      return { stampSet: f.stampSet.id, amount: f.amount };
    });
    state.latestBought = props.latestBought.map(f => {
      return { stampSet: f.stampSet.id, amount: f.amount };
    });
  }
};
