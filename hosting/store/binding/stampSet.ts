import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IBindingStampSetState, IState } from "~/store";
import { StampSetRef } from "tomato-puree/client/ref";
import {
  IStampSetDocument,
  IStampSetItemDocument
} from "tomato-puree/client/firestoreDocType";

type IModuleState = IBindingStampSetState;

/*
 * state
 */

export const state = (): IModuleState => {
  return {};
};

/*
 * getters
 */

export const getters: GetterTree<IModuleState, IState> = {
  data(state: IModuleState): (stampSetId: string) => IStampSetDocument | null {
    return stampSetId => {
      return state[stampSetId];
    };
  },
  stampItem(
    state: IModuleState
  ): (stampSetId: string, stampItemId: string) => IStampSetItemDocument | null {
    return stampSetId => {
      const stampSetDoc = state[stampSetId];
      if (!stampSetDoc) {
        return null;
      }

      return stampSetDoc.items[stampSetId];
    };
  }
};

/*
 * actions
 */

export const actions: ActionTree<IModuleState, IState> = {
  async bind({ state, commit }, props: { stampSetRef: StampSetRef }) {
    // stampSet is not be `bind`, load it only first time
    const { stampSetRef } = props;
    const key = stampSetRef.ref.id;
    if (!key || state[key]) {
      return;
    }

    const value = await stampSetRef.data();
    commit("STORE_STAMP_SET_OBJECT", { key, value });
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<IModuleState> = {
  STORE_STAMP_SET_OBJECT: (
    state,
    props: { key: string; value: IStampSetDocument }
  ) => {
    Vue.set(state, props.key, props.value);
  }
};
