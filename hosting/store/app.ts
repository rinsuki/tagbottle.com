import { firebaseAction } from "vuexfire";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IAppState, IState } from "./index";
import {
  nestedList,
  INestedTag
} from "tomato-puree/util/convert/tag/nestedList";
import { RefGenerator } from "tomato-puree/client/ref";

/*
 * state
 */

export const state = (): IAppState => ({
  authInitialized: false,
  firebaseInitialized: false,
  bookmark: null,
  currentUser: null,
  notification: null
});

export const getters: GetterTree<IAppState, IState> = {
  appInitialized: state => state.firebaseInitialized && state.authInitialized,
  bookmark: state => (state.bookmark ? state.bookmark : { tags: [] }),
  currentUser: state => state.currentUser,
  notification: state =>
    state.notification ? state.notification : { topics: [], tokens: [] },

  nested(state, getters): INestedTag[] {
    const tags = getters.bookmark.tags;
    const bookmarkTags = Object.keys(tags).filter(tag => tags[tag]);
    return nestedList(bookmarkTags);
  }
};

export const actions: ActionTree<IAppState, IState> = {
  auth() {
    const provider = new this.$firebase.auth.GoogleAuthProvider();
    this.$firebase.auth().signInWithRedirect(provider);
  },

  async signOut({ state, dispatch }) {
    await this.$firebase.auth().signOut();
    if (state.currentUser) {
      await dispatch("setCurrentUserBinding", { ref: null });
    }
  },

  async setCurrentUser({ state, commit, dispatch }, { user, token }) {
    // if null is passed, unbind existing ref
    if (!user) {
      const promises: Promise<any>[] = [];
      if (state.currentUser) {
        promises.push(dispatch("setCurrentUserBinding", { ref: null }));
      }
      if (state.bookmark) {
        promises.push(dispatch("setBookmarkBinding", { ref: null }));
      }
      if (state.notification) {
        promises.push(dispatch("setNotificationBinding", { ref: null }));
      }
      await Promise.all(promises);

      setTimeout(() => commit("AUTH_INITIALIZED"), 3000);
      return;
    }

    // create ref. to user doc
    const db = this.$firebase.firestore();
    const userRef = db.collection("/users").doc(user.uid);
    await dispatch("setCurrentUserBinding", { ref: userRef });

    // create user doc for first login
    if (!state.currentUser) {
      commit("AUTH_INITIALIZED");

      const data = {
        displayName: user.displayName,
        photoUrl: user.photoURL,
        token
      };
      await this.$firebase.functions().httpsCallable("app-createUser")(data);
    }

    // bind required docs
    const refGenerator = new RefGenerator(db);
    const promises: Promise<any>[] = [];

    const bookmarkRef = db.collection("/bookmarks").doc(user.uid);
    promises.push(dispatch("setBookmarkBinding", { ref: bookmarkRef }));

    const notificationRef = db.collection("/notifications").doc(user.uid);
    promises.push(dispatch("setNotificationBinding", { ref: notificationRef }));

    // const stampStockRef = refGenerator.stam
    const stampStockRef = refGenerator.stampStock(user.uid);
    promises.push(
      dispatch("binding/stampStock/bind", { stampStockRef }, { root: true })
    );

    await Promise.all(promises);
    commit("AUTH_INITIALIZED");
  },

  async requestNotifications() {
    // request permission
    const messaging = this.$firebase.messaging();
    await messaging.requestPermission();

    // save token
    const token = await messaging.getToken();
    const user = this.$firebase.auth().currentUser;
    await this.$firebase
      .firestore()
      .collection("/notifications")
      .doc(user.uid)
      .set({ tokens: { [token]: true } }, { merge: true });
  },

  async toggleBookmarkTag(context, { tag, enable }) {
    const user = this.$firebase.auth().currentUser;
    await this.$firebase
      .firestore()
      .collection("/bookmarks")
      .doc(user.uid)
      .set({ tags: { [tag]: enable } }, { merge: true });
  },

  async toggleTopicNotification({ dispatch }, { themes, enable }) {
    await dispatch("requestNotifications");
    const user = this.$firebase.auth().currentUser;
    const topic = encodeURIComponent(themes.join("/")).replace(
      /[!'()*]/g,
      c => {
        return "%" + c.charCodeAt(0).toString(16);
      }
    );
    await this.$firebase
      .firestore()
      .collection("/notifications")
      .doc(user.uid)
      .set({ topics: { [topic]: enable } }, { merge: true });
  },
  async changeIcon(_, { filePath }) {
    if (!filePath) {
      return;
    }
    // upload icon file
    const user = this.$firebase.auth().currentUser.uid;
    const storageRef = this.$firebase.storage().ref();
    const iconRef = storageRef.child(`icons/${user}/icon`);
    const uploadTask = iconRef.put(filePath);
    // set icon url
    uploadTask.on(
      "state_changed",
      snapshot => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error: Error) => {
        alert(error.message);
      },
      async () => {
        await this.$firebase
          .firestore()
          .collection("/users")
          .doc(user)
          .update({ photoUrl: null });
      }
    );
  },

  async changeDisplayName(_, { displayName }) {
    if (!displayName) {
      return;
    }
    const user = this.$firebase.auth().currentUser.uid;
    await this.$firebase
      .firestore()
      .collection("/users")
      .doc(user)
      .update({ displayName });
  },

  async changeUsername(_, { username }) {
    if (!username) {
      return;
    }
    await this.$firebase
      .firestore()
      .collection("/usernames")
      .doc(username)
      .set({ user: this.$firebase.auth().currentUser.uid });
  },

  /*
  firebase binding
  */

  setCurrentUserBinding: firebaseAction(
    async ({ bindFirebaseRef, unbindFirebaseRef, commit }, { ref }) => {
      const key = "currentUser";
      if (!ref) {
        await unbindFirebaseRef(key);
        commit("REMOVE_CURRENT_USER");
      } else {
        await bindFirebaseRef(key, ref);
      }
    }
  ),
  setBookmarkBinding: firebaseAction(
    async ({ bindFirebaseRef, unbindFirebaseRef, commit }, { ref }) => {
      const key = "bookmark";
      if (!ref) {
        await unbindFirebaseRef(key);
        commit("REMOVE_BOOKMARK");
      } else {
        await bindFirebaseRef(key, ref);
      }
    }
  ),
  setNotificationBinding: firebaseAction(
    async ({ bindFirebaseRef, unbindFirebaseRef, commit }, { ref }) => {
      const key = "notification";
      if (!ref) {
        await unbindFirebaseRef(key);
        commit("REMOVE_NOTIFICATION");
      } else {
        await bindFirebaseRef(key, ref);
      }
    }
  )
};

export const mutations: MutationTree<IAppState> = {
  FIREBASE_INITIALIZED(state) {
    state.firebaseInitialized = true;
  },
  AUTH_INITIALIZED(state) {
    state.authInitialized = true;
  },

  REMOVE_BOOKMARK(state) {
    state.bookmark = null;
  },
  REMOVE_CURRENT_USER(state) {
    state.currentUser = null;
  },
  REMOVE_NOTIFICATION(state) {
    state.notification = null;
  }
};
