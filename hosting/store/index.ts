import * as DocTypes from "tomato-puree/client/firestoreDocType";
import { firebaseMutations } from "vuexfire";

export const mutations = {
  ...firebaseMutations
};

export interface IState {
  app: IAppState;
  chat: IChatState;
  tag: ITagState;
  notification: INotificationState;
  bindig: {
    bottleMessages: IBindingBottleMessagesState;
    tag: IBindingTagState;
  };
}

/*
  app.ts
 */

export interface IAppState {
  authInitialized: boolean;
  firebaseInitialized: boolean;
  bookmark: any;
  currentUser: any;
  notification: any;
}

/*
  chat.ts
 */

export interface IChatState {
  themes: string[];
  loading: boolean;
  bottleIndex: number | null;
  bottlePath: string | null;
  currentIndex: number | null;
}

/*
  notification.ts
 */

export interface INotificationState {
  notifications: any[];
}

/*
  tag.ts
 */

export interface ITagState {}

/*
  binding/bottleMessages.ts
 */

export interface IBindingBottleMessagesState {
  [key: string]: IBindMessageData[] | null;
}
export interface IBindMessageData {
  id: string;
  index: number;
  text: string;
  timestamp: DocTypes.CommonType.ITimestamp;
  stamps: object;
  themes: string[];
  userId: string;
}

/*
  binding/stampSet.ts
 */

export interface IBindingStampSetState {
  [key: string]: DocTypes.IStampSetDocument | null;
}

/*
  binding/stampStock.ts
 */
export interface IBindingStampStockState extends DocTypes.IStampStock {
  favorites: Array<{ stampSet: string; amount: number }>;
  latestUsed: Array<{ stampSet: string; amount: number }>;
  latestBought: Array<{ stampSet: string; amount: number }>;
}

/*
  binding/tag.ts
 */

export interface IBindingTagState {
  [key: string]: IBindTagData | null;
}
export interface IBindTagData extends DocTypes.ITag {
  currentBottle: string;
  currentIndex: number;
  bottles: { [key: number]: string };
}

/*
  binding/user.ts
 */

export interface IBindingUserState {
  [key: string]: IBindUserData | null;
}
export interface IBindUserData extends DocTypes.IUserDocument {}
