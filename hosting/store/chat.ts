import * as clientPuree from "tomato-puree/client";
import isInvalidThemes from "tomato-puree/util/validate/tag/isInvalidThemes";
import themesForTag from "tomato-puree/util/convert/tag/themesForTag";
import RefGenerator = clientPuree.Ref.RefGenerator;

import { firebaseAction } from "vuexfire";
import * as firebase from "firebase/app";
import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IState, IChatState, IBindTagData, IBindMessageData } from "./index";
import { TagRef } from "tomato-puree/client/ref/pureeRef";
import { IStampDocument } from "tomato-puree/client/firestoreDocType";

/*
 * state
 */

export const state = (): IChatState => ({
  themes: [],
  loading: true,
  currentIndex: null,
  bottleIndex: null,
  bottlePath: null
});

/*
 * getters
 */

export interface IMessageData extends IBindMessageData {
  loaded: boolean;
}
export interface IBottlesData {
  index: number;
  bottlePath: string;
}

export const getters: GetterTree<IChatState, IState> = {
  messages(state, getters, rootState, rootGetters): IMessageData[] {
    if (state.loading) {
      return [];
    }

    const bindMessages: IBindMessageData[] =
      rootGetters["binding/bottleMessages/data"](state.bottlePath) || [];

    const result: IMessageData[] = [];
    for (const bindMessage of bindMessages) {
      let loaded = true;

      const user = rootGetters["binding/user/data"](bindMessage.userId);

      // check user binding
      loaded = loaded && !!user;

      // loaded = loaded && true;
      const message: IMessageData = Object.assign({}, bindMessage, {
        user,
        loaded
      });
      result.unshift(message);
    }

    // sort desc order with timestamp
    // if timestamp is null, treat them as latest messages
    return result.sort((a, b) => {
      const left = a.timestamp;
      const right = b.timestamp;
      if (left && right) {
        return right.seconds - left.seconds;
      } else if (left && !right) {
        return 1;
      } else if (!left && right) {
        return -1;
      } else {
        return 0;
      }
    });
  },

  bottles(state, getters, rootState, rootGetters): IBottlesData[] {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;

    const result: IBottlesData[] = [];
    if (tag && tag.bottles) {
      const keys = Object.keys(tag.bottles);
      keys.sort();
      for (const key of keys) {
        const index = +key;
        const bottlePath = tag.bottles[key];
        result.push({ index, bottlePath });
      }
    }

    // desc order
    return result.reverse();
  },

  index(state, getters): number {
    // TODO: this should be done by firestore callback?
    const messages = getters["messages"];
    const messagesCount = messages.length;
    const biggestIndex = Math.max(...messages.map(m => m.index));
    return Math.max(messagesCount, biggestIndex);
  },

  currentBottleIndex(state, getters, rootState, rootGetters): number | null {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;
    if (tag) {
      return tag.currentIndex;
    } else {
      return state.bottleIndex;
    }
  },
  isLatestBottle(state, getters, rootState, rootGetters): boolean {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;
    if (tag) {
      return state.bottleIndex === tag.currentIndex;
    } else {
      return true;
    }
  },

  stamps(state) {
    return state[`stamps-${state.bottlePath}`] || {};
  },

  hasOlderMessagesOfCurrentBottle(
    state,
    getters,
    rootState,
    rootGetters
  ): boolean {
    return !rootGetters["binding/bottleMessages/isAllBind"](
      state.bottlePath
    ) as boolean;
  }
};

export const actions: ActionTree<IChatState, IState> = {
  async selectThemes({ state, commit, dispatch }, { themes }) {
    // replace selected tag if given one is invalid
    if (!isInvalidThemes(themes)) {
      commit("SELECT_THEMES", { themes });
    } else if (themes.length === 0) {
      // or use default value, if empty themes selected
      commit("SELECT_THEMES", { themes: ["hello", "world"] });
    }

    await dispatch("tag/addTag", { themes: state.themes }, { root: true });
    await dispatch("startChatByThemes", { themes: state.themes });
  },

  async sendMessage({ state, getters }, { themes, text }) {
    // message needs bottle param
    if (!state.bottlePath) {
      return;
    }

    //  create message
    const db = this.$firebase.firestore();
    const user = this.$firebase
      .firestore()
      .collection("/users")
      .doc(this.$firebase.auth().currentUser.uid);
    const stamps = [];
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const bottle = db.doc(state.bottlePath);
    const index = getters["index"] + 1;
    await this.$firebase
      .firestore()
      .collection("/messages")
      .add({ user, index, stamps, text, themes, timestamp, bottle });

    // rebottle (if needed)
    if (index >= 200) {
      // TODO: remove this dirty workaround...
      const bottleId = state.bottlePath.split("/")[1];
      await this.$firebase.functions().httpsCallable("chat-rebottle")({
        bottleId
      });
    }
  },

  async editMessage({ state }, { messageId, text }) {
    // TODO: when dispatched, the edited message are disappear from timeline
    await this.$firebase
      .firestore()
      .collection("/messages")
      .doc(messageId)
      .update({ text });
  },

  async sendStamp(
    { state },
    props: { messageId: string; stampSetId: string; stampItemId: string }
  ) {
    if (!state.bottlePath) {
      return;
    }
    const { messageId, stampSetId, stampItemId } = props;

    // create refs
    const db = this.$firebase.firestore();
    const refGenerator = new RefGenerator(db);

    const userRef = refGenerator.user(this.$firebase.auth().currentUser.uid);
    const stampSetRef = refGenerator.stampSet(stampSetId);
    const messageDocRef = db.collection("messages").doc(messageId);
    const newStampRef = refGenerator.newStamp();

    if (!userRef) {
      return;
    }

    // use stamp
    const data: IStampDocument = {
      user: userRef.ref,
      stampSet: stampSetRef.ref,
      message: messageDocRef,
      stampItemId,
      status: "pending"
    };
    await newStampRef.set(data);
  },

  async startChatByThemes(
    { commit, state, dispatch, getters, rootGetters },
    { attempt = 0, themes }: { attempt: 0 | 1 | 2 | 3; themes: string[] }
  ) {
    // return if `themes` is null
    if (!themes) {
      console.warn("startChatByThemes null themes");
      return;
    }

    // check auth status
    if (!this.$firebase.auth().currentUser) {
      console.warn("startChatByThemes auth:retry");
      setTimeout(() => dispatch("startChatByThemes", { themes, attempt }), 100);
      return;
    }
    if (attempt > 3) {
      throw "startChatByThemes: attempt 4";
    }

    // set up ref
    const db = this.$firebase.firestore();
    const refManager = new RefGenerator(db);
    const tagRef = refManager.tag(themes) as TagRef;

    // create tag document if it does not exist yet
    // check only at first time
    const tag = rootGetters["binding/tag/data"](themes) as IBindTagData;
    const invalidTag = !tag || !tag.currentBottle;

    // start load
    if (invalidTag) {
      commit("LOADING_STATUS", { loading: true });
    }

    if (attempt == 0 && invalidTag) {
      let existing = (await tagRef.ref.get()).exists;
      if (!existing) {
        // create
        const createTagFunction = this.$firebase
          .functions()
          .httpsCallable("chat-createTag");
        await createTagFunction({ themes });
      }

      // bind tag (first time only)
      await dispatch(
        "binding/tag/bind",
        { themes, tagRef: tagRef.ref },
        { root: true }
      );

      // dispatch next try
      setTimeout(
        () => dispatch("startChatByThemes", { themes, attempt: 1 }),
        100
      );

      // continue
      return;
    }

    // wait until tag-bind
    if (invalidTag) {
      console.warn("startChatByThemes tag:bind:retry");
      setTimeout(() => dispatch("startChatByThemes", { themes, attempt }), 100);
      return;
    }

    // set message query
    const bottleRef = db.doc(tag.currentBottle);
    await dispatch(
      "binding/bottleMessages/bind",
      { bottleRef },
      { root: true }
    );

    // save bottle info
    const bottlePath = bottleRef.path;
    const bottleIndex = tag.currentIndex;
    commit("SAVE_BOTTLE_INFO", { bottlePath, bottleIndex });

    // end load
    commit("LOADING_STATUS", { loading: false });
  },

  async startChatByTag({ dispatch }, { tag }) {
    // convert tag string to themes
    const themes = themesForTag(tag);
    await dispatch("startChatByThemes", { themes });
  },

  async selectBottleIndex(
    { commit, dispatch, getters, state, rootGetters },
    { index }
  ) {
    const tag: IBindTagData | null = rootGetters["binding/tag/data"](
      state.themes
    );
    if (!tag || !tag.bottles) {
      console.warn("selectBottleIndex failed");
      return;
    }

    const bottlePath = tag.bottles[index];

    // set message query
    const db = this.$firebase.firestore();
    const bottleRef = db.doc(bottlePath);
    await dispatch(
      "binding/bottleMessages/bind",
      { bottleRef },
      { root: true }
    );

    commit("SAVE_BOTTLE_INFO", { bottlePath, bottleIndex: index });
  },

  async loadAllMessagesOfCurrentBottle({ state, dispatch }) {
    const db = this.$firebase.firestore();

    const bottleRef = db.doc(state.bottlePath);

    await dispatch(
      "binding/bottleMessages/bindAll",
      { bottleRef },
      { root: true }
    );
  }
};

export const mutations: MutationTree<IChatState> = {
  SELECT_THEMES(state, { themes }) {
    state.themes = themes;
  },
  SAVE_BOTTLE_INFO(state, { bottlePath, bottleIndex }) {
    state.bottlePath = bottlePath;
    state.bottleIndex = bottleIndex;
  },
  LOADING_STATUS(state, props: { loading: boolean }) {
    state.loading = props.loading;
  }
};
