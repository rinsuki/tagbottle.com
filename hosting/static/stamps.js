export default {
  stampStock: {
    latestBought: [
      {
        amount: 100,
        stampSet: "1pHo5gKklUPEj1dLj9py"
      }
    ]
  },
  stampSets: {
    "1pHo5gKklUPEj1dLj9py": {
      vendor: "ef",
      title: "ef stamp",
      items: [
        {
          id: "ef-01",
          index: 1,
          name: "01",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F01.png?alt=media&token=4249e747-bd5a-42cc-8a2f-8a9e305a6c75"
        },
        {
          id: "ef-02",
          index: 2,
          name: "02",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F02.png?alt=media&token=dd335823-ce5f-4e82-89b6-4a1bba330529"
        },
        {
          id: "ef-03",
          index: 3,
          name: "03",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F03.png?alt=media&token=df371b99-eb00-435c-856f-db3f9c1e2091"
        },
        {
          id: "ef-04",
          index: 4,
          name: "04",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F04.png?alt=media&token=c23fcb15-cc62-498a-a6c1-a353ef14486d"
        },
        {
          id: "ef-05",
          index: 5,
          name: "05",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F05.png?alt=media&token=cd978b9a-ff40-4b9b-a177-0899489aa137"
        },
        {
          id: "ef-06",
          index: 6,
          name: "06",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F06.png?alt=media&token=208c1b37-74d5-4975-a474-c28dc76be90a"
        },
        {
          id: "ef-07",
          index: 7,
          name: "07",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F07.png?alt=media&token=a5201b5f-6e05-4a4b-9104-05a7ad57b639"
        },
        {
          id: "ef-08",
          index: 8,
          name: "08",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F08.png?alt=media&token=b1f13e54-dee0-4519-ab21-216bb371b921"
        },
        {
          id: "ef-09",
          index: 9,
          name: "09",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F09.png?alt=media&token=02b45411-9390-4f82-a297-2d236b746926"
        },
        {
          id: "ef-10",
          index: 10,
          name: "10",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F10.png?alt=media&token=df08c9a4-ddfc-41b8-960b-5262452a503c"
        },
        {
          id: "ef-11",
          index: 11,
          name: "11",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F11.png?alt=media&token=45364a16-3c94-4ff6-b8bc-960df3c90c8e"
        },
        {
          id: "ef-12",
          index: 12,
          name: "12",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F12.png?alt=media&token=4ef42d86-d6db-4d24-bbfa-f02ac5c637a3"
        },
        {
          id: "ef-13",
          index: 13,
          name: "13",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F13.png?alt=media&token=e0e644a2-8b6a-4de8-805f-6c701ba4b694"
        },
        {
          id: "ef-14",
          index: 14,
          name: "14",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F14.png?alt=media&token=ab3fb719-eaeb-4340-97fb-54c835adb011"
        },
        {
          id: "ef-15",
          index: 15,
          name: "15",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F15.png?alt=media&token=c4f7f075-cd79-43ae-b175-3c7578e82c60"
        },
        {
          id: "ef-16",
          index: 16,
          name: "16",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F16.png?alt=media&token=b6b828ed-007b-4d0e-800d-d498887cbcba"
        },
        {
          id: "ef-17",
          index: 17,
          name: "17",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F17.png?alt=media&token=5226dd13-8ce5-42db-965a-0d6006f0d008"
        },
        {
          id: "ef-18",
          index: 18,
          name: "18",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F18.png?alt=media&token=1e48ca3c-d65e-4f28-8c96-acbf27118ecd"
        },
        {
          id: "ef-19",
          index: 19,
          name: "19",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F19.png?alt=media&token=3c25d77a-1d99-4ee4-9003-62193385f4e3"
        },
        {
          id: "ef-20",
          index: 20,
          name: "20",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F20.png?alt=media&token=6d234a13-2d6f-4365-99e5-9b207b8731d0"
        },
        {
          id: "ef-21",
          index: 21,
          name: "21",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F21.png?alt=media&token=c49299cb-0d4f-42f0-b112-3ce400b97c0d"
        },
        {
          id: "ef-22",
          index: 22,
          name: "22",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F22.png?alt=media&token=2daef7b2-66db-4e68-9d12-f0036437c8e7"
        },
        {
          id: "ef-23",
          index: 23,
          name: "23",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F23.png?alt=media&token=704439b8-f6dd-41ea-8c51-dd070e727067"
        },
        {
          id: "ef-24",
          index: 24,
          name: "24",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F24.png?alt=media&token=4c43168d-fd81-41b1-8ff2-f78560525014"
        },
        {
          id: "ef-25",
          index: 25,
          name: "25",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F25.png?alt=media&token=7e9ece4d-b54d-4b62-afd9-3d96ce00f65d"
        },
        {
          id: "ef-26",
          index: 26,
          name: "26",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F26.png?alt=media&token=7a90c6b0-8d4f-49b0-943e-4dcfb2517b0f"
        },
        {
          id: "ef-27",
          index: 27,
          name: "27",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F27.png?alt=media&token=d5c27015-c838-49b8-a1bc-743f16bf40c3"
        },
        {
          id: "ef-28",
          index: 28,
          name: "28",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F28.png?alt=media&token=b7d6ed9b-a478-4933-9ef3-04f6b4842fe7"
        },
        {
          id: "ef-29",
          index: 29,
          name: "29",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F29.png?alt=media&token=d287eb33-d4f2-4383-aa8f-2c18d970c4df"
        },
        {
          id: "ef-30",
          index: 30,
          name: "30",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F30.png?alt=media&token=ba8a9d59-5f9b-4562-b60e-1162711ab619"
        },
        {
          id: "ef-31",
          index: 31,
          name: "31",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F31.png?alt=media&token=76b43679-f911-4e92-98f4-114cc825b7a0"
        },
        {
          id: "ef-32",
          index: 32,
          name: "32",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F32.png?alt=media&token=32079ac6-bba4-4a00-825d-812495a8e9b5"
        },
        {
          id: "ef-33",
          index: 33,
          name: "33",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F33.png?alt=media&token=9296768e-6fca-4bc6-9ba4-f85fd553bf46"
        },
        {
          id: "ef-34",
          index: 34,
          name: "34",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F34.png?alt=media&token=963902d4-03ac-40f8-9123-0e3ef06d791f"
        },
        {
          id: "ef-35",
          index: 35,
          name: "35",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F35.png?alt=media&token=f54fa1af-9e59-4ac6-b1c9-ce89ab20b166"
        },
        {
          id: "ef-36",
          index: 36,
          name: "36",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F36.png?alt=media&token=a123d823-3be0-4620-9fa6-a2b2c52aae03"
        },
        {
          id: "ef-37",
          index: 37,
          name: "37",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F37.png?alt=media&token=c3ff4f43-f5c5-441a-9728-0d47a2d1c758"
        },
        {
          id: "ef-38",
          index: 38,
          name: "38",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F38.png?alt=media&token=f61ba12e-a088-43bb-a47f-2e09d4233ff4"
        },
        {
          id: "ef-39",
          index: 39,
          name: "39",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F39.png?alt=media&token=b9053ea1-8b90-4c4e-a9f9-64b305eda8cf"
        },
        {
          id: "ef-40",
          index: 40,
          name: "40",
          url:
            "https://firebasestorage.googleapis.com/v0/b/tagbottle-dev-2a1e1.appspot.com/o/stamps%2Fef%2F40.png?alt=media&token=51d2b61d-e035-455c-9b0c-eaa83ac845b9"
        }
      ]
    }
  }
}
