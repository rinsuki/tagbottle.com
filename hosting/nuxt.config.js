const parseArgs = require("minimist")
const argv = parseArgs(process.argv.slice(2), {
  alias: {
    H: "hostname",
    p: "port"
  },
  string: ["H"],
  unknown: () => false
})

const port =
  argv.port ||
  process.env.PORT ||
  process.env.npm_package_config_nuxt_port ||
  "3000"
const host =
  argv.hostname ||
  process.env.HOST ||
  process.env.npm_package_config_nuxt_host ||
  "localhost"

module.exports = {
  env: {
    baseUrl: process.env.BASE_URL || `http://${host}:${port}`
  },
  head: {
    title: "firebase-web",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "web client for tagbottle.com"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css"
      }
    ],
    script: []
  },

  /*
  ** Build configuration
  */

  build: {
    publicPath: "/assets/",
    extractCSS: true,
    babel: {
      plugins: [
        [
          "@babel/plugin-transform-runtime",
          {
            corejs: 2,
            regenerator: true
          }
        ]
      ]
    },
    postcss: {
      plugins: {
        // to stop warning with bulma https://github.com/nuxt/nuxt.js/issues/1670
        "postcss-custom-properties": false
      }
    }
  },

  plugins: [
    "~/plugins/buefy.js",
    "~/plugins/firebase.js",
    "~/plugins/textarea-autosize.js",
    { src: "~/plugins/markdown/markdown-it", ssr: false },
    { src: "~/plugins/vue-notification", ssr: false }
  ],

  css: [
    { src: 'highlightjs/styles/atom-one-light.css', lang: 'css' }
  ],

  modules: [
    "~/modules/typescript.js",
    "@nuxtjs/pwa",
    [
      "nuxt-i18n",
      {
        locales: [
          { code: "ja", iso: "ja-JP", file: "ja-JP.js", name: "日本語" }
        ],
        defaultLocale: "ja",
        lazy: true,
        langDir: "lang/"
      }
    ]
  ],

  // pwa module
  manifest: {
    name: "tagbottle",
    lang: "ja",
    gcm_sender_id: "103953800507"
  }
}
