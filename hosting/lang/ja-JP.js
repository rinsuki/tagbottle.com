export default {
  top: {
    content: {
      subtitle: "テーマで繋がるSNS",
      titleForTag: "タグ",
      descriptionTag: "気になるテーマについて自由にタグを作りましょう。",
      titleForChat: "メッセージ",
      descriptionChat:
        "タグのページにメッセージを投稿して他の人と交流しましょう。",
      titleForStamp: "スタンプ",
      descriptionStamp: "気に入ったメッセージにスタンプを押しましょう。"
    },
    buttonForLogin: "Googleアカウントでログイン",
    buttonForChat: "メッセージを送る"
  },

  navigation: {
    labelForChat: "メッセージ",
    labelForDocument: "ドキュメント",
    labelForMenu: "メニュー",
    labelForProfile: "プロフィール",
    labelForStamp: "スタンプ (実装中)",
    labelForTag: "タグ",
    labelForLogin: "ログイン",
    labelForLogout: "ログアウト",
    placeholderForSelectTag: "タグ/作成/例",
    buttonForSelectTag: "作成"
  },

  tagSummary: {
    label: "タグ情報",
    addTag: "タグを登録",
    placeholder: "ボトルを選択",
    bookmarkTag: "ブックマーク"
  },

  bookmark: {
    label: "ブックマーク"
  },

  chat: {
    send: {
      buttonForPost: "送信",
      placeholderForTextInput: "メッセージを入力してください...",
      failedDueToStringLength: "入力可能な文字数は1〜2048文字です",
      failedDueToOldBottle: "最新のボトルに移動してください",
      tooltipForPreview: "プレビュー",
      tooltipForEmoji: "絵文字"
    },
    edit: {
      labelForEditMessage: "メッセージを編集",
      buttonForCancel: "キャンセル",
      buttonForChange: "変更"
    },
    emoji: {
      buttonForCancel: "キャンセル"
    },
    stamp: {
      labelForSelectStamp: "スタンプを選択",
      buttonForCancel: "キャンセル"
    },
    notification: {
      label: "最近見たタグ (実装中)"
    },
    displayAllMessages: "全てのメッセージを表示する",
    moveToLatestBottle: "最新のボトルに移動する"
  },

  profile: {
    edit: {
      title: "プロフィール更新",

      labelForIcon: "アイコン",
      uploadIcon: "アイコンを変更",
      succeedChangingIcon:
        "アイコンを変更しました。反映されるまでしばらく時間がかかります。",
      failedToChangeIcon: "アイコンの変更に失敗しました",

      labelForDisplayName: "表示名",
      changeDisplayName: "表示名を変更",
      succeedChangingDisplayName: "表示名を変更しました",
      failedToChangeDisplayName: "表示名の変更に失敗しました",

      labelForUsername: "ユーザー名 (アルファベットと数字とアンダースコアのみ)",
      changeUsername: "ユーザー名を変更",
      invalidUsernameWarning: "有効なユーザー名ではありません",
      confirmToChangeUsername:
        "ユーザー名を変更すると、現在のユーザー名は他人に取得されてしまうかもしれません。ユーザー名を変更しますか？",
      succeedChangingUsername: "ユーザー名を変更しました",
      failedToChangeUsername: "ユーザー名を取得できませんでした"
    }
  }
};
