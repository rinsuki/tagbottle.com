import MarkdownIt from "markdown-it";
import MarkdownItEmoji from "markdown-it-emoji";
import hljs from "highlightjs";
import { tagRule, tagRender } from "./tag";
import { videoRule, videoRender } from "./video";
import { emojis } from "~/static/emojis";

export default async ({ store }, inject) => {
  // init
  const md = new MarkdownIt({
    html: false,
    breaks: true,
    linkify: true,
    linkTarget: "_blank",
    highlight: function(str, lang) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(lang, str).value;
        } catch (err) {
          return "";
        }
      }
      try {
        return hljs.highlightAuto(str).value;
      } catch (err) {
        return "";
      }
    },
    typographer: false
  });

  // open links in new tab/window
  // https://github.com/markdown-it/markdown-it/blob/master/docs/architecture.md#renderer
  const defaultRender =
    md.renderer.rules.link_open ||
    function(tokens, idx, options, env, self) {
      return self.renderToken(tokens, idx, options);
    };
  md.renderer.rules.link_open = function(tokens, idx, options, env, self) {
    // If you are sure other plugins can't add `target` - drop check below
    const aIndex = tokens[idx].attrIndex("target");
    if (aIndex < 0) {
      tokens[idx].attrPush(["target", "_blank"]); // add new attribute
    } else {
      tokens[idx].attrs[aIndex][1] = "_blank"; // replace value of existing attr
    }
    // pass token to default renderer.
    return defaultRender(tokens, idx, options, env, self);
  };

  // add tag rule to markdown-it
  md.inline.ruler.push("tag", tagRule);
  md.renderer.rules.tag = tagRender;

  // add video rule to markdown-it
  md.inline.ruler.before("escape", "video", videoRule);
  md.renderer.rules.video = videoRender;

  // enable emoji
  const options = { enabled: Object.keys(emojis) };
  md.use(MarkdownItEmoji, options);
  md.renderer.rules.emoji = function(token, idx) {
    return `<img src="${emojis[token[idx].markup]}" class="emoji">`;
  };

  // inject
  inject("md", md);
};
