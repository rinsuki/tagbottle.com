import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/messaging";
import "firebase/storage";
import config from "./firebaseConfig.json";

export default async ({ store }, inject) => {
  // init
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
    firebase.firestore().settings({
      timestampsInSnapshots: true
    });
  }

  // messages
  if (process.client) {
    try {
      firebase
        .messaging()
        .usePublicVapidKey(
          "BAo3N4Maxe_yBBnBoxUZFpSzNP_M1ZNnof_tIdLKVutp9Jixb1-HYmAY6x_P57nR--bE4fCbZn-0xsfARWYZZmQ"
        );
    } catch (e) {
      if (!localStorage.getItem("alreadyFcmFailed")) {
        alert(
          "FCM初期化に失敗しました。Safariでは新着メッセージ通知を受け取ることができません :("
        );
        localStorage.setItem("alreadyFcmFailed", 1);
      }
    }
  }

  // offline data
  if (process.client) {
    try {
      // offline data
      // https://firebase.google.com/docs/firestore/manage-data/enable-offline?authuser=0
      await firebase.firestore().enablePersistence();
    } catch (e) {
      if (e.code === "failed-precondition") {
        // Multiple tabs open, persistence can only be enabled
        // in one tab at a a time.
        console.log("offline data error: failed-precondition");
      } else if (e.code === "unimplemented") {
        // The current browser does not support all of the
        // features required to enable persistence
        console.log("offline data error: unimplemented");
      }
    }
  }

  // auth
  firebase.auth().onAuthStateChanged(
    user => {
      store.dispatch("app/setCurrentUser", { user });
    },
    error => {
      if (process.client) {
        alert(error);
      }
      store.dispatch("app/setCurrentUser", { user: null });
    }
  );

  inject("firebase", firebase);
  store.commit("app/FIREBASE_INITIALIZED");
};
