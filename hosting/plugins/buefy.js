import Vue from "vue";
import Buefy from "buefy";
import "~/assets/buefy.scss";

Vue.use(Buefy);
